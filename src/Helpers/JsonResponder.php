<?php

namespace Dupkey\Helpers;

use Psr\Http\Message\ResponseInterface;

/**
 * Some JSON functions to reduce repetition
 */
class JsonResponder
{
    /**
     * Provide an informational response
     *
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @param   array   $data
     * @param   string  $message
     * @param   int     $code     3 digits
     *
     * @return  \Psr\Http\Message\ResponseInterface
     */
    public function informational($response, $data = [], $message = null, $code = 100)
    {
        $data         = $data ? ['data' => $data] : [];
        $message      = $message ? ['message' => $message] : [];
        $responseBody = json_encode(array_merge($message, $data));

        return $response->withStatus($code)
            ->withHeader('Content-type', 'application/json')
            ->write($responseBody);
    }

    /**
     * Provide a success response
     *
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @param   array   $data
     * @param   string  $message
     * @param   int     $code     3 digits
     *
     * @return  \Psr\Http\Message\ResponseInterface
     */
    public function success($response, $data = [], $message = null, $code = 200)
    {
        $data         = $data ? ['data' => $data] : [];
        $message      = $message ? ['message' => $message] : [];
        $responseBody = json_encode(array_merge($message, $data));

        return $response->withStatus($code)
            ->withHeader('Content-type', 'application/json')
            ->write($responseBody);
    }

    /**
     * Provide a redirect response
     *
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @param   string  $uri
     * @param   int     $code    3 digits
     *
     * @return  \Psr\Http\Message\ResponseInterface
     */
    public function redirect($response, $uri, $code = 300)
    {
        return $response->withStatus($code)
            ->withHeader('Location', $uri);
    }

    /**
     * Provide a failure json reponse
     *
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @param   string  $message
     * @param   array   $data
     * @param   int     $code     3 digits
     *
     * @return  \Psr\Http\Message\ResponseInterface
     */
    public function fail($response, $message, $data = [], $code = 400)
    {
        $message      = $message ? ['message' => $message] : [];
        $data         = $data ? ['data' => $data] : [];
        $responseBody = json_encode(array_merge($message, $data));

        return $response->withStatus($code)
            ->withHeader('Content-type', 'application/json')
            ->write($responseBody);
    }

    /**
     * Provide an error response
     *
     * @param  \Psr\Http\Message\ResponseInterface $response
     * @param   string  $message
     * @param   int     $code     3 digits
     *
     * @return  \Psr\Http\Message\ResponseInterface
     */
    public function error($response, $message, $code = 500)
    {
        $message      = $message ? ['message' => $message] : [];
        $responseBody = json_encode($message);

        return $response->withStatus($code)
            ->withHeader('Content-type', 'application/json')
            ->write($responseBody);
    }
}
